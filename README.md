# tcp_client_with_joystick

Soket programlama ile sunucu yazılıma bağlanan ve sunucuya, joystickten aldığı verileri gönderen C/C++ dilleri ile yazılmış ve paralel olarak çalışan istemci yazılımdır.

## Derleme İşlemi

derle.sh çalıştırılması yeterlidir. Çıktı olarak "client" isimli çalıştırılabilir dosya üretir.

## Çalıştırma

./client komutunun verilmesi yeterlidir. Başlangıçta bağlanılacak port bilgisi istenecektir.
