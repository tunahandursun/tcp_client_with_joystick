#include "joystick.hh"
#include <unistd.h>

int main(int argc, char** argv){
        Joystick joystick("/dev/input/js0");
        if (!joystick.isFound()){
                printf("open failed.\n");
                exit(1);
        }
        JoystickEvent event;
        while (true){
                usleep(1000);
                if (joystick.sample(&event)){
		        
		printf("#####");
                        if (event.isButton()){
                                printf("Button %u is %s\n",event.number,event.value == 0 ? "up" : "down");
                        }
                        else if (event.isAxis())
                        {
                                printf("Axis %u is at position %d\n", event.number, event.value);
                        }
                }  
        } 
}
