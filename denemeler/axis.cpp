#include "joystick.hh"
#include <unistd.h>

int main(){
	Joystick joystick("/dev/input/js0");
	if(!joystick.isFound()){
		printf("openda hata\n");
		exit(1);
	}
	JoystickEvent event;
	while(true){
		usleep(1000);
		if(joystick.sample(&event)){
			if(event.isButton())
				printf("buton girişi: %u is %s\n",event.number,event.value==0?"up":"down");
			if(event.isAxis())
				printf("axis girişi: eventnumber %u value %d\n",event.number,event.value);
		}
		
	}
}
