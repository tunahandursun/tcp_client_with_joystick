#include<iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <string.h>
#include <unistd.h>
#include "joystick.hh"

using namespace std;

#define bufferuzunluk 1

int main(int argc, char *argv[]){
                Joystick joystick("/dev/input/js0");
                if (!joystick.isFound()){
                                printf("open failed.\n");
                                exit(1);
                }
                int sockfd, portno, n;

                struct sockaddr_in serv_addr;
                struct hostent *server;

                char buffer[bufferuzunluk];
                if (argc < 3) {
                   fprintf(stderr,"usage %s hostname port\n", argv[0]);
                   return 0;
                }
                portno = atoi(argv[2]);
                sockfd = socket(AF_INET, SOCK_STREAM, 0);
                if (sockfd < 0) {
                                printf("ERROR opening socket");
                                return 0;
                }
                server = gethostbyname(argv[1]);
                if (server == NULL) {
                                fprintf(stderr,"ERROR, no such host\n");
                                close(sockfd);
                                return 0;
                }
                bzero((char *) &serv_addr, sizeof(serv_addr));
                serv_addr.sin_family = AF_INET;
                bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr,server->h_length);
                serv_addr.sin_port = htons(portno);
                if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
                                printf("ERROR connecting");
                                close(sockfd);
                                return 0;
                }
                while(true){
                        bzero(buffer,bufferuzunluk);
                                usleep(1000);
                                JoystickEvent event;
                                if (joystick.sample(&event)){
				  if (event.isButton() &&  event.value != 0){ //event.number
                                            cout<<"yazılacak değer: "<< event.number <<endl;
                                            if(event.number == 0){
	                                                n = write(sockfd,"0",0);
                                                    if (n < 0){
                                                            printf("ERROR writing to socket");
                                                            close(sockfd);
                                                            return 0;
                                                    }
                                            }
                                            else if(event.number == 1){
								n = write(sockfd,"1",0);
		                                                    if (n < 0){
                	                                            printf("ERROR writing to socket");
                        	                                    close(sockfd);
                                	                            return 0;
						   		 }
                                            }
                                            else if(event.number == 2){
                                                	n = write(sockfd,"2",1);
                                                    if (n < 0){
                                                            printf("ERROR writing to socket");
                                                            close(sockfd);
                                                            return 0;
                                                    }
                                            }
                                            else if(event.number == 3){
                                                n = write(sockfd,"3",1);
                                                    if (n < 0){
                                                            printf("ERROR writing to socket");
                                                            close(sockfd);
                                                            return 0;
                                                    }

                                            }
                                            else if(event.number == 4){
                                                n = write(sockfd,"4",1);
                                                    if (n < 0){
                                                            printf("ERROR writing to socket");
                                                            close(sockfd);
                                                            return 0;
                                                    }
                                            }
                                            else if(event.number == 5){
                                                n = write(sockfd,"5",1);
                                                    if (n < 0){
                                                            printf("ERROR writing to socket");
                                                            close(sockfd);
                                                            return 0;
                                                    }
                                            }
                                            else if(event.number == 6){
                                                n = write(sockfd,"6",1);
                                                    if (n < 0){
                                                            printf("ERROR writing to socket");
                                                            close(sockfd);
                                                            return 0;
                                                    }
                                            }
                                            else if(event.number == 7){
                                                n = write(sockfd,"7",1);
                                                    if (n < 0){
                                                            printf("ERROR writing to socket");
                                                            close(sockfd);
                                                            return 0;
                                                    }
                                            }
                                            else
                                                cout<<"bu tanımlı değil\n";
                                        }
                                        else
                                                cout<<"buton değil\n";
                                }

                }
                close(sockfd);
                return 0;
}
